package com.sageburner.im.android.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;

import com.sageburner.im.android.BootstrapApplication;
import com.sageburner.im.android.Injector;
import com.sageburner.im.android.util.Ln;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.Views;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * Base class for all Bootstrap Activities that need fragments.
 */
public class BootstrapFragmentActivity extends ActionBarActivity {

    @Inject
    protected Bus eventBus;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Injector.inject(this);
    }

    @Override
    public void setContentView(final int layoutResId) {
        super.setContentView(layoutResId);

        Views.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        eventBus.register(this);
    }

    @Override
    protected void onPause() {
        try {
            // Store benchmark output
            PrintStream out;
            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                // Store data in the external storage
                File file = new File(Environment.getExternalStorageDirectory(), "sb_im_crypto_benchmark.out");
                //File file = new File(getExternalFilesDir(null), "sb_im_crypto_benchmark.out");
                out = new PrintStream(new FileOutputStream(file));
            } else {
                // Store date in the internal storage
                out = new PrintStream(openFileOutput("sb_im_crypto_benchmark.out", Context.MODE_WORLD_READABLE));
            }

            String perfLog = BootstrapApplication.getGlobalSB().toString();
            out.print(perfLog);
            out.flush();
        } catch (Exception e) {
            String fail = "Failure!";
            Ln.d("MainActivity::onDestroy: ", fail);
            e.printStackTrace();
        }

        super.onPause();
        eventBus.unregister(this);
    }
}
